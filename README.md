# Wikidata import

It may be necessary to 
```
pip3 install --upgrade setuptools
```

### SPARQL query

```sparql
prefix wdt:   <http://www.wikidata.org/prop/direct/>
prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#>
prefix skos:  <http://www.w3.org/2004/02/skos/core#>

construct {
    ?bdrcitem wdt:P2477 ?bdrc ;
            ?labelp ?wdbdrcL ;
            wdt:P214 ?bdrcviaf .
} WHERE { 
	{
    	?bdrcitem wdt:P2477 ?bdrc .
        VALUES ?labelp { rdfs:label skos:altLabel }
      	?bdrcitem ?labelp ?wdbdrcL .
    	FILTER(lang(?wdbdrcL) = "en" || lang(?wdbdrcL) = "zh")
    } union {
        ?bdrcitem wdt:P2477 ?bdrc ;
                  wdt:P214 ?bdrcviaf .
    }
}
```

(I don't understand why but combining the two queries makes the Wikidata SPARQL endpoint return invalid ttl)

```sh
curl -XPOST -H "Accept: text/turtle" -o wdresults.ttl https://query.wikidata.org/sparql -d 'query=prefix%20wdt%3A%20%20%20%3Chttp%3A%2F%2Fwww.wikidata.org%2Fprop%2Fdirect%2F%3E%0Aprefix%20rdfs%3A%20%20%3Chttp%3A%2F%2Fwww.w3.org%2F2000%2F01%2Frdf-schema%23%3E%0Aprefix%20skos%3A%20%20%3Chttp%3A%2F%2Fwww.w3.org%2F2004%2F02%2Fskos%2Fcore%23%3E%0A%0Aconstruct%20%7B%0A%20%20%20%20%3Fbdrcitem%20wdt%3AP2477%20%3Fbdrc%20%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20%3Flabelp%20%3FwdbdrcL%20%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20wdt%3AP214%20%3Fbdrcviaf%20.%0A%7D%20WHERE%20%7B%20%0A%09%7B%0A%20%20%20%20%20%20%20%20%3Fbdrcitem%20wdt%3AP2477%20%3Fbdrc%20%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20wdt%3AP214%20%3Fbdrcviaf%20.%0A%20%20%20%20%7D%0A%7D'

curl -XPOST -H "Accept: text/turtle" -o wdresults-labels.ttl https://query.wikidata.org/sparql -d 'query=prefix%20wdt%3A%20%20%20%3Chttp%3A%2F%2Fwww.wikidata.org%2Fprop%2Fdirect%2F%3E%0Aprefix%20rdfs%3A%20%20%3Chttp%3A%2F%2Fwww.w3.org%2F2000%2F01%2Frdf-schema%23%3E%0Aprefix%20skos%3A%20%20%3Chttp%3A%2F%2Fwww.w3.org%2F2004%2F02%2Fskos%2Fcore%23%3E%0A%0Aconstruct%20%7B%0A%20%20%20%20%3Fbdrcitem%20rdfs%3Alabel%20%3FwdbdrcL%20%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20wdt%3AP214%20%3Fbdrcviaf%20.%0A%7D%20WHERE%20%7B%20%0A%09%7B%0A%20%20%20%20%09%3Fbdrcitem%20wdt%3AP2477%20%3Fbdrc%20.%0A%20%20%20%20%20%20%09%3Fbdrcitem%20rdfs%3Alabel%20%3FwdbdrcL%20.%0A%20%20%20%20%09FILTER(lang(%3FwdbdrcL)%20%3D%20%22en%22%20%7C%7C%20lang(%3FwdbdrcL)%20%3D%20%22zh%22)%0A%20%20%20%20%7D%0A%7D'

python3 import.py

curl -X PUT -H Content-Type:text/turtle -T WDImport.ttl -G http://buda1.bdrc.io:13180/fuseki/corerw/data --data-urlencode 'graph=http://purl.bdrc.io/graph/WDImport'

curl -X POST http://purl.bdrc.io/clearcache
```



#### idRef

PREFIX foaf: <http://xmlns.com/foaf/0.1/> 

construct { ?p ?pp ?po } { ?p <http://purl.org/dc/terms/language> <http://lexvo.org/id/iso639-3/bod> ; a foaf:Person ; ?pp ?po } 
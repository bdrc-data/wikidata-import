import rdflib
from rdflib.namespace import RDF, RDFS, SKOS, OWL, Namespace, NamespaceManager, XSD, URIRef
import csv
import pathlib
import pyewts

converter = pyewts.pyewts()

BDR = Namespace("http://purl.bdrc.io/resource/")
BDO = Namespace("http://purl.bdrc.io/ontology/core/")
BDA = Namespace("http://purl.bdrc.io/admindata/")
BDG = Namespace("http://purl.bdrc.io/graph/")
ADM = Namespace("http://purl.bdrc.io/ontology/admin/")
WD = Namespace("http://www.wikidata.org/entity/")
WDT = Namespace("http://www.wikidata.org/prop/direct/")
DILA = Namespace("http://purl.dila.edu.tw/resource/")
VIAF = Namespace("http://viaf.org/viaf/")

NSM = NamespaceManager(rdflib.Graph())
NSM.bind("bdr", BDR)
NSM.bind("bdg", BDG)
NSM.bind("bdo", BDO)
NSM.bind("bda", BDA)
NSM.bind("adm", ADM)
NSM.bind("skos", SKOS)
NSM.bind("rdfs", RDFS)
NSM.bind("wd", WD)
NSM.bind("owl", OWL)
NSM.bind("wdt", WDT)
NSM.bind("dila", DILA)
NSM.bind("viaf", VIAF)

def get_withdrawnlist():
    res = {}
    with open('ridReplacements.csv', newline='') as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        for row in reader:
            res[row[0]] = True
    return res

# see https://github.com/RDFLib/rdflib/issues/806
x = rdflib.term._toPythonMapping.pop(rdflib.XSD['gYear'])

def add_ext_file(g, filename, format="turtle", lowercasetags=False):
    g.parse(filename, format=format)

def migrate(g, withdrawnlist):
    for s,p,o in g.triples( (None,  RDFS.label, None) ):
        g.remove((s,p,o))
        if o.language == "zh":
            newo = rdflib.Literal(o.value, lang="zh-hani")
            g.add((s,SKOS.prefLabel,newo))
        elif o.language == "bo":
            newo = rdflib.Literal(converter.toWylie(o.value), lang="bo-x-ewts")
            g.add((s,SKOS.prefLabel,newo))
        else:
            g.add((s,SKOS.prefLabel,o))
    for s,p,o in g.triples( (None,  SKOS.altLabel, None) ):
        g.remove((s,p,o))
        if o.language == "zh":
            newo = rdflib.Literal(o.value, lang="zh-hani")
            g.add((s,SKOS.altLabel,newo))
        elif o.language == "bo":
            newo = rdflib.Literal(converter.toWylie(o.value), lang="bo-x-ewts")
            g.add((s,SKOS.altLabel,newo))
        else:
            g.add((s,SKOS.altLabel,o))
    for s,p,o in g.triples( (None,  WDT.P2477, None) ):
        # same as BDRC
        g.remove((s,p,o))
        if str(o) in withdrawnlist:
            continue
        newo = BDR[str(o)]
        if str(o).startswith('P'):
            g.add((s,RDF.type,BDO.Person))
        if str(o).startswith('G'):
            g.add((s,RDF.type,BDO.Place))
        g.add((s,OWL.sameAs,newo))
        g.add((newo,OWL.sameAs,s))
        #g.add((s,ADM.sameAsBDRC,newo))
        g.add((BDA.WDImport, ADM.adminAbout, s))
    for s,p,o in g.triples( (None,  WDT.P1187, None) ):
        # sameas DILA
        g.remove((s,p,o))
        newo = DILA[str(o)]
        if str(o).startswith('A'):
            g.add((s,RDF.type,BDO.Person))
        g.add((s,OWL.sameAs,newo))
        g.add((newo,OWL.sameAs,s))
        #g.add((s,BDO.sameAsDILA,newo))
        g.add((BDA.WDImport, ADM.adminAbout, s))
    for s,p,o in g.triples( (None,  WDT.P214, None) ):
        # sameas VIAF
        g.remove((s,p,o))
        newo = VIAF[str(o)]
        g.add((s,OWL.sameAs,newo))
    # add transitive sameAs
    for s,_,o in g.triples( (None,  OWL.sameAs, None) ):
        if str(s).startswith("http://purl.bdrc"):
            for _,_,o2 in g.triples( (o,  OWL.sameAs, None) ):
                if o2 != s:
                    if str(o2).startswith("http://purl.bdrc"):
                        print("duplication problem: %s and %s and %s" % (str(s), str(o2), str(o)))
                    else:
                        g.add((s,OWL.sameAs,o2))


def viafcsv(g):
    with open('viaf.csv', newline='') as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        for row in reader:
            g.add((BDR[row[0]], OWL.sameAs, VIAF[row[1]]))

def main():
    g = rdflib.Graph()
    add_ext_file(g, "wdresults.ttl", "turtle")
    add_ext_file(g, "wdresults-labels.ttl", "turtle")
    withdrawnlist = get_withdrawnlist()
    migrate(g, withdrawnlist)
    viafcsv(g)
    add_ext_file(g, "static.ttl", "turtle")
    g.namespace_manager = NSM
    g.serialize("WDImport.ttl", format="turtle")

if __name__ == "__main__":
    main()